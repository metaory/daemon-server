/**
 * Created by pou on 7/23/2014.
 */

var restify = require('restify');

var server = restify.createServer({
    name: 'myapp',
    version: '1.0.0'
});
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.get('/echo/:name', function (req, res, next) {
    res.send("vola");
//    res.send(req.params);
    return next();
});

server.post('/api/log',function(req,res,next){
console.log('req',req.body)
})

server.listen(8080, function () {
    console.log('%s listening at %s', server.name, server.url);
});
