Daemon Server
=========

Nodejs Server daemon

  - work with rest api
  - store in mongo db


Requirement
--------------


install [NodeJs](http://nodejs.org/)



Installation
--------------

```sh
git clone https://metaory@bitbucket.org/metaory/daemon-server.git
cd daemon-server
npm install
node server

```



License
----

MIT


**Free Software, Hell Yeah!**

[Pou Yan]:metaory@gmail.com